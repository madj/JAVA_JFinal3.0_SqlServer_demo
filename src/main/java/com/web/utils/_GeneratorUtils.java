package com.web.utils;

import javax.sql.DataSource;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.druid.DruidPlugin;

/**
 * 本 demo 仅表达最为粗浅的 jfinal 用法，更为有价值的实用的企业级用法
 * 详见 JFinal 俱乐部: http://jfinal.com/club
 * 
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _GeneratorUtils {
	
	protected MetaBuilder metaBuilder;

	public static DataSource getDataSource() {
		
		PropKit.use("SqlserverConfigLocal.txt");
		DruidPlugin druidPlugin = createDruidPlugin();
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}
	
	public static DruidPlugin createDruidPlugin() {
		
		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim(), PropKit.get("driver"));
	}

	public static void main(String[] args) {
		
		// base model 所使用的包名
		String baseModelPackageName = "com.web.remote.model";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/web/remote/model";
		
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.web.remote.dao";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/web/remote/dao";
		
		// 创建生成器
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		
		// 重写MetaBuilder方法
		generator.setMetaBuilder(new _MetaBuilderUtils(getDataSource()));
		
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 添加不需要生成的表名
		generator.addExcludedTable("CHECK_CONSTRAINTS");
		generator.addExcludedTable("COLUMN_DOMAIN_USAGE");
		generator.addExcludedTable("COLUMN_PRIVILEGES");
		generator.addExcludedTable("COLUMNS");
		generator.addExcludedTable("CONSTRAINT_COLUMN_USAGE");
		generator.addExcludedTable("CONSTRAINT_TABLE_USAGE");
		generator.addExcludedTable("DOMAIN_CONSTRAINTS");
		generator.addExcludedTable("DOMAINS");
		generator.addExcludedTable("KEY_COLUMN_USAGE");
		generator.addExcludedTable("PARAMETERS");
		generator.addExcludedTable("REFERENTIAL_CONSTRAINTS");
		generator.addExcludedTable("ROUTINE_COLUMNS");
		generator.addExcludedTable("ROUTINES");
		generator.addExcludedTable("SCHEMATA");
		generator.addExcludedTable("TABLE_CONSTRAINTS");
		generator.addExcludedTable("TABLE_PRIVILEGES");
		generator.addExcludedTable("TABLES");
		generator.addExcludedTable("VIEW_COLUMN_USAGE");
		generator.addExcludedTable("VIEW_TABLE_USAGE");
		generator.addExcludedTable("VIEWS");
		generator.addExcludedTable("all_columns");
		generator.addExcludedTable("all_objects");
		generator.addExcludedTable("all_parameters");
		generator.addExcludedTable("all_sql_modules");
		generator.addExcludedTable("all_views");
		generator.addExcludedTable("allocation_units");
		generator.addExcludedTable("assemblies");
		generator.addExcludedTable("assembly_files");
		generator.addExcludedTable("assembly_modules");
		generator.addExcludedTable("assembly_references");
		generator.addExcludedTable("assembly_types");
		generator.addExcludedTable("asymmetric_keys");
		generator.addExcludedTable("backup_devices");
		generator.addExcludedTable("certificates");
		generator.addExcludedTable("change_tracking_databases");
		generator.addExcludedTable("change_tracking_tables");
		generator.addExcludedTable("check_constraints");
		generator.addExcludedTable("column_type_usages");
		generator.addExcludedTable("column_xml_schema_collection_usages");
		generator.addExcludedTable("columns");
		generator.addExcludedTable("computed_columns");
		generator.addExcludedTable("configurations");
		generator.addExcludedTable("conversation_endpoints");
		generator.addExcludedTable("conversation_groups");
		generator.addExcludedTable("conversation_priorities");
		generator.addExcludedTable("credentials");
		generator.addExcludedTable("crypt_properties");
		generator.addExcludedTable("cryptographic_providers");
		generator.addExcludedTable("data_spaces");
		generator.addExcludedTable("database_audit_specification_details");
		generator.addExcludedTable("database_audit_specifications");
		generator.addExcludedTable("database_files");
		generator.addExcludedTable("database_mirroring");
		generator.addExcludedTable("database_mirroring_endpoints");
		generator.addExcludedTable("database_mirroring_witnesses");
		generator.addExcludedTable("database_permissions");
		generator.addExcludedTable("database_principal_aliases");
		generator.addExcludedTable("database_principals");
		generator.addExcludedTable("database_recovery_status");
		generator.addExcludedTable("database_role_members");
		generator.addExcludedTable("databases");
		generator.addExcludedTable("default_constraints");
		generator.addExcludedTable("destination_data_spaces");
		generator.addExcludedTable("dm_audit_actions");
		generator.addExcludedTable("dm_audit_class_type_map");
		generator.addExcludedTable("dm_broker_activated_tasks");
		generator.addExcludedTable("dm_broker_connections");
		generator.addExcludedTable("dm_broker_forwarded_messages");
		generator.addExcludedTable("dm_broker_queue_monitors");
		generator.addExcludedTable("dm_cdc_errors");
		generator.addExcludedTable("dm_cdc_log_scan_sessions");
		generator.addExcludedTable("dm_clr_appdomains");
		generator.addExcludedTable("dm_clr_loaded_assemblies");
		generator.addExcludedTable("dm_clr_properties");
		generator.addExcludedTable("dm_clr_tasks");
		generator.addExcludedTable("dm_cryptographic_provider_properties");
		generator.addExcludedTable("dm_database_encryption_keys");
		generator.addExcludedTable("dm_db_file_space_usage");
		generator.addExcludedTable("dm_db_index_usage_stats");
		generator.addExcludedTable("dm_db_mirroring_auto_page_repair");
		generator.addExcludedTable("dm_db_mirroring_connections");
		generator.addExcludedTable("dm_db_mirroring_past_actions");
		generator.addExcludedTable("dm_db_missing_index_details");
		generator.addExcludedTable("dm_db_missing_index_group_stats");
		generator.addExcludedTable("dm_db_missing_index_groups");
		generator.addExcludedTable("dm_db_partition_stats");
		generator.addExcludedTable("dm_db_persisted_sku_features");
		generator.addExcludedTable("dm_db_script_level");
		generator.addExcludedTable("dm_db_session_space_usage");
		generator.addExcludedTable("dm_db_task_space_usage");
		generator.addExcludedTable("dm_exec_background_job_queue");
		generator.addExcludedTable("dm_exec_background_job_queue_stats");
		generator.addExcludedTable("dm_exec_cached_plans");
		generator.addExcludedTable("dm_exec_connections");
		generator.addExcludedTable("dm_exec_procedure_stats");
		generator.addExcludedTable("dm_exec_query_memory_grants");
		generator.addExcludedTable("dm_exec_query_optimizer_info");
		generator.addExcludedTable("dm_exec_query_resource_semaphores");
		generator.addExcludedTable("dm_exec_query_stats");
		generator.addExcludedTable("dm_exec_query_transformation_stats");
		generator.addExcludedTable("dm_exec_requests");
		generator.addExcludedTable("dm_exec_sessions");
		generator.addExcludedTable("dm_exec_trigger_stats");
		generator.addExcludedTable("dm_filestream_file_io_handles");
		generator.addExcludedTable("dm_filestream_file_io_requests");
		generator.addExcludedTable("dm_fts_active_catalogs");
		generator.addExcludedTable("dm_fts_fdhosts");
		generator.addExcludedTable("dm_fts_index_population");
		generator.addExcludedTable("dm_fts_memory_buffers");
		generator.addExcludedTable("dm_fts_memory_pools");
		generator.addExcludedTable("dm_fts_outstanding_batches");
		generator.addExcludedTable("dm_fts_population_ranges");
		generator.addExcludedTable("dm_io_backup_tapes");
		generator.addExcludedTable("dm_io_cluster_shared_drives");
		generator.addExcludedTable("dm_io_pending_io_requests");
		generator.addExcludedTable("dm_os_buffer_descriptors");
		generator.addExcludedTable("dm_os_child_instances");
		generator.addExcludedTable("dm_os_cluster_nodes");
		generator.addExcludedTable("dm_os_dispatcher_pools");
		generator.addExcludedTable("dm_os_dispatchers");
		generator.addExcludedTable("dm_os_hosts");
		generator.addExcludedTable("dm_os_latch_stats");
		generator.addExcludedTable("dm_os_loaded_modules");
		generator.addExcludedTable("dm_os_memory_allocations");
		generator.addExcludedTable("dm_os_memory_brokers");
		generator.addExcludedTable("dm_os_memory_cache_clock_hands");
		generator.addExcludedTable("dm_os_memory_cache_counters");
		generator.addExcludedTable("dm_os_memory_cache_entries");
		generator.addExcludedTable("dm_os_memory_cache_hash_tables");
		generator.addExcludedTable("dm_os_memory_clerks");
		generator.addExcludedTable("dm_os_memory_node_access_stats");
		generator.addExcludedTable("dm_os_memory_nodes");
		generator.addExcludedTable("dm_os_memory_objects");
		generator.addExcludedTable("dm_os_memory_pools");
		generator.addExcludedTable("dm_os_nodes");
		generator.addExcludedTable("dm_os_performance_counters");
		generator.addExcludedTable("dm_os_process_memory");
		generator.addExcludedTable("dm_os_ring_buffers");
		generator.addExcludedTable("dm_os_schedulers");
		generator.addExcludedTable("dm_os_spinlock_stats");
		generator.addExcludedTable("dm_os_stacks");
		generator.addExcludedTable("dm_os_sublatches");
		generator.addExcludedTable("dm_os_sys_info");
		generator.addExcludedTable("dm_os_sys_memory");
		generator.addExcludedTable("dm_os_tasks");
		generator.addExcludedTable("dm_os_threads");
		generator.addExcludedTable("dm_os_virtual_address_dump");
		generator.addExcludedTable("dm_os_wait_stats");
		generator.addExcludedTable("dm_os_waiting_tasks");
		generator.addExcludedTable("dm_os_windows_info");
		generator.addExcludedTable("dm_os_worker_local_storage");
		generator.addExcludedTable("dm_os_workers");
		generator.addExcludedTable("dm_qn_subscriptions");
		generator.addExcludedTable("dm_repl_articles");
		generator.addExcludedTable("dm_repl_schemas");
		generator.addExcludedTable("dm_repl_tranhash");
		generator.addExcludedTable("dm_repl_traninfo");
		generator.addExcludedTable("dm_resource_governor_configuration");
		generator.addExcludedTable("dm_resource_governor_resource_pools");
		generator.addExcludedTable("dm_resource_governor_workload_groups");
		generator.addExcludedTable("dm_server_audit_status");
		generator.addExcludedTable("dm_server_memory_dumps");
		generator.addExcludedTable("dm_server_registry");
		generator.addExcludedTable("dm_server_services");
		generator.addExcludedTable("dm_tran_active_snapshot_database_transactions");
		generator.addExcludedTable("dm_tran_active_transactions");
		generator.addExcludedTable("dm_tran_commit_table");
		generator.addExcludedTable("dm_tran_current_snapshot");
		generator.addExcludedTable("dm_tran_current_transaction");
		generator.addExcludedTable("dm_tran_database_transactions");
		generator.addExcludedTable("dm_tran_locks");
		generator.addExcludedTable("dm_tran_session_transactions");
		generator.addExcludedTable("dm_tran_top_version_generators");
		generator.addExcludedTable("dm_tran_transactions_snapshot");
		generator.addExcludedTable("dm_tran_version_store");
		generator.addExcludedTable("dm_xe_map_values");
		generator.addExcludedTable("dm_xe_object_columns");
		generator.addExcludedTable("dm_xe_objects");
		generator.addExcludedTable("dm_xe_packages");
		generator.addExcludedTable("dm_xe_session_event_actions");
		generator.addExcludedTable("dm_xe_session_events");
		generator.addExcludedTable("dm_xe_session_object_columns");
		generator.addExcludedTable("dm_xe_session_targets");
		generator.addExcludedTable("dm_xe_sessions");
		generator.addExcludedTable("endpoint_webmethods");
		generator.addExcludedTable("endpoints");
		generator.addExcludedTable("event_notification_event_types");
		generator.addExcludedTable("event_notifications");
		generator.addExcludedTable("events");
		generator.addExcludedTable("extended_procedures");
		generator.addExcludedTable("extended_properties");
		generator.addExcludedTable("filegroups");
		generator.addExcludedTable("foreign_key_columns");
		generator.addExcludedTable("foreign_keys");
		generator.addExcludedTable("fulltext_catalogs");
		generator.addExcludedTable("fulltext_document_types");
		generator.addExcludedTable("fulltext_index_catalog_usages");
		generator.addExcludedTable("fulltext_index_columns");
		generator.addExcludedTable("fulltext_index_fragments");
		generator.addExcludedTable("fulltext_indexes");
		generator.addExcludedTable("fulltext_languages");
		generator.addExcludedTable("fulltext_stoplists");
		generator.addExcludedTable("fulltext_stopwords");
		generator.addExcludedTable("fulltext_system_stopwords");
		generator.addExcludedTable("function_order_columns");
		generator.addExcludedTable("http_endpoints");
		generator.addExcludedTable("identity_columns");
		generator.addExcludedTable("index_columns");
		generator.addExcludedTable("indexes");
		generator.addExcludedTable("internal_tables");
		generator.addExcludedTable("key_constraints");
		generator.addExcludedTable("key_encryptions");
		generator.addExcludedTable("linked_logins");
		generator.addExcludedTable("login_token");
		generator.addExcludedTable("master_files");
		generator.addExcludedTable("master_key_passwords");
		generator.addExcludedTable("message_type_xml_schema_collection_usages");
		generator.addExcludedTable("messages");
		generator.addExcludedTable("module_assembly_usages");
		generator.addExcludedTable("numbered_procedure_parameters");
		generator.addExcludedTable("numbered_procedures");
		generator.addExcludedTable("objects");
		generator.addExcludedTable("openkeys");
		generator.addExcludedTable("parameter_type_usages");
		generator.addExcludedTable("parameter_xml_schema_collection_usages");
		generator.addExcludedTable("parameters");
		generator.addExcludedTable("partition_functions");
		generator.addExcludedTable("partition_parameters");
		generator.addExcludedTable("partition_range_values");
		generator.addExcludedTable("partition_schemes");
		generator.addExcludedTable("partitions");
		generator.addExcludedTable("plan_guides");
		generator.addExcludedTable("procedures");
		generator.addExcludedTable("remote_logins");
		generator.addExcludedTable("remote_service_bindings");
		generator.addExcludedTable("resource_governor_configuration");
		generator.addExcludedTable("resource_governor_resource_pools");
		generator.addExcludedTable("resource_governor_workload_groups");
		generator.addExcludedTable("routes");
		generator.addExcludedTable("schemas");
		generator.addExcludedTable("securable_classes");
		generator.addExcludedTable("server_assembly_modules");
		generator.addExcludedTable("server_audit_specification_details");
		generator.addExcludedTable("server_audit_specifications");
		generator.addExcludedTable("server_audits");
		generator.addExcludedTable("server_event_notifications");
		generator.addExcludedTable("server_event_session_actions");
		generator.addExcludedTable("server_event_session_events");
		generator.addExcludedTable("server_event_session_fields");
		generator.addExcludedTable("server_event_session_targets");
		generator.addExcludedTable("server_event_sessions");
		generator.addExcludedTable("server_events");
		generator.addExcludedTable("server_file_audits");
		generator.addExcludedTable("server_permissions");
		generator.addExcludedTable("server_principal_credentials");
		generator.addExcludedTable("server_principals");
		generator.addExcludedTable("server_role_members");
		generator.addExcludedTable("server_sql_modules");
		generator.addExcludedTable("server_trigger_events");
		generator.addExcludedTable("server_triggers");
		generator.addExcludedTable("servers");
		generator.addExcludedTable("service_broker_endpoints");
		generator.addExcludedTable("service_contract_message_usages");
		generator.addExcludedTable("service_contract_usages");
		generator.addExcludedTable("service_contracts");
		generator.addExcludedTable("service_message_types");
		generator.addExcludedTable("service_queue_usages");
		generator.addExcludedTable("service_queues");
		generator.addExcludedTable("services");
		generator.addExcludedTable("soap_endpoints");
		generator.addExcludedTable("spatial_index_tessellations");
		generator.addExcludedTable("spatial_indexes");
		generator.addExcludedTable("spatial_reference_systems");
		generator.addExcludedTable("sql_dependencies");
		generator.addExcludedTable("sql_expression_dependencies");
		generator.addExcludedTable("sql_logins");
		generator.addExcludedTable("sql_modules");
		generator.addExcludedTable("stats");
		generator.addExcludedTable("stats_columns");
		generator.addExcludedTable("symmetric_keys");
		generator.addExcludedTable("synonyms");
		generator.addExcludedTable("sysaltfiles");
		generator.addExcludedTable("syscacheobjects");
		generator.addExcludedTable("syscharsets");
		generator.addExcludedTable("syscolumns");
		generator.addExcludedTable("syscomments");
		generator.addExcludedTable("sysconfigures");
		generator.addExcludedTable("sysconstraints");
		generator.addExcludedTable("syscurconfigs");
		generator.addExcludedTable("syscursorcolumns");
		generator.addExcludedTable("syscursorrefs");
		generator.addExcludedTable("syscursors");
		generator.addExcludedTable("syscursortables");
		generator.addExcludedTable("sysdatabases");
		generator.addExcludedTable("sysdepends");
		generator.addExcludedTable("sysdevices");
		generator.addExcludedTable("sysfilegroups");
		generator.addExcludedTable("sysfiles");
		generator.addExcludedTable("sysforeignkeys");
		generator.addExcludedTable("sysfulltextcatalogs");
		generator.addExcludedTable("sysindexes");
		generator.addExcludedTable("sysindexkeys");
		generator.addExcludedTable("syslanguages");
		generator.addExcludedTable("syslockinfo");
		generator.addExcludedTable("syslogins");
		generator.addExcludedTable("sysmembers");
		generator.addExcludedTable("sysmessages");
		generator.addExcludedTable("sysobjects");
		generator.addExcludedTable("sysoledbusers");
		generator.addExcludedTable("sysopentapes");
		generator.addExcludedTable("sysperfinfo");
		generator.addExcludedTable("syspermissions");
		generator.addExcludedTable("sysprocesses");
		generator.addExcludedTable("sysprotects");
		generator.addExcludedTable("sysreferences");
		generator.addExcludedTable("sysremotelogins");
		generator.addExcludedTable("sysservers");
		generator.addExcludedTable("system_columns");
		generator.addExcludedTable("system_components_surface_area_configuration");
		generator.addExcludedTable("system_internals_allocation_units");
		generator.addExcludedTable("system_internals_partition_columns");
		generator.addExcludedTable("system_internals_partitions");
		generator.addExcludedTable("system_objects");
		generator.addExcludedTable("system_parameters");
		generator.addExcludedTable("system_sql_modules");
		generator.addExcludedTable("system_views");
		generator.addExcludedTable("systypes");
		generator.addExcludedTable("sysusers");
		generator.addExcludedTable("table_types");
		generator.addExcludedTable("tables");
		generator.addExcludedTable("tcp_endpoints");
		generator.addExcludedTable("trace_categories");
		generator.addExcludedTable("trace_columns");
		generator.addExcludedTable("trace_event_bindings");
		generator.addExcludedTable("trace_events");
		generator.addExcludedTable("trace_subclass_values");
		generator.addExcludedTable("traces");
		generator.addExcludedTable("transmission_queue");
		generator.addExcludedTable("trigger_event_types");
		generator.addExcludedTable("trigger_events");
		generator.addExcludedTable("triggers");
		generator.addExcludedTable("type_assembly_usages");
		generator.addExcludedTable("types");
		generator.addExcludedTable("user_token");
		generator.addExcludedTable("via_endpoints");
		generator.addExcludedTable("views");
		generator.addExcludedTable("xml_indexes");
		generator.addExcludedTable("xml_schema_attributes");
		generator.addExcludedTable("xml_schema_collections");
		generator.addExcludedTable("xml_schema_component_placements");
		generator.addExcludedTable("xml_schema_components");
		generator.addExcludedTable("xml_schema_elements");
		generator.addExcludedTable("xml_schema_facets");
		generator.addExcludedTable("xml_schema_model_groups");
		generator.addExcludedTable("xml_schema_namespaces");
		generator.addExcludedTable("xml_schema_types");
		generator.addExcludedTable("xml_schema_wildcard_namespaces");
		generator.addExcludedTable("xml_schema_wildcards");
		// 设置是否在 Model 中生成 dao 对象
		generator.setGenerateDaoInModel(true);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		generator.setRemovedTableNamePrefixes("aa_");
		// 设置数据库方言
		generator.setDialect(new SqlServerDialect());

		// 执行生成
		generator.generate();

	}

}
