package com.web.utils;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

public class _MetaBuilderUtils extends MetaBuilder {

	public _MetaBuilderUtils(DataSource dataSource) {
		super(dataSource);
	}

	protected String buildBaseModelName(String modelName) {
		return modelName + "Model";
	}

}
